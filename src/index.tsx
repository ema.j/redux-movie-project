import ReactDOM from "react-dom/client";
import App from "./App";
import { Provider } from "react-redux";
import { applyMiddleware, createStore } from "redux";
import reducers from "./redux/reducers";
import thunk from "redux-thunk";
import "./index.scss";

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);

const store = createStore(reducers, applyMiddleware(thunk));

export type Store = ReturnType<typeof reducers>;

root.render(
  <Provider store={store}>
    <App />
  </Provider>
);
