import { MoviesResults } from "../redux/actions/action-types-movies";
import { Link } from "react-router-dom";
import { Store } from "../index";
import { useSelector } from "react-redux";
import "./Movies.scss";

const MovieCard = () => {
  const state = useSelector((state: Store) => state.allMovies);

  return (
    <>
      <div className="cards">
        {state.movie ? (
          state.movie?.results.map((movie: MoviesResults) => {
            return (
              <div key={movie.id} className="card">
                <img
                  src={`https://image.tmdb.org/t/p/w200/${movie.poster_path}`}
                  alt="poster"
                  className="card-img"
                />
                <div className="card-content">
                  <div className="vote">
                    <span className="vote-percentage">{`${
                      movie.vote_average * 10
                    }%`}</span>
                  </div>
                  <Link to={`movie/${movie.id}`} className="card-content-title">
                    <h4>{movie.title}</h4>
                  </Link>
                  <p className="card-content-release-date">
                    {movie.release_date}
                  </p>
                </div>
              </div>
            );
          })
        ) : (
          <div className="error">{state.error}</div>
        )}
      </div>
    </>
  );
};

export default MovieCard;
