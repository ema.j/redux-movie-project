import { useSelector } from "react-redux";
import { Store } from "../index";
import { Genres, SpokenLanguages } from "../redux/actions/action-types-details";
import { IoIosLink } from "react-icons/io";
import NumberFormat from "react-number-format";
import "./Details.scss";

const Details = () => {
  const movie = useSelector((state: Store) => state.movieDetails);
  const error = useSelector((state: Store) => state.movieDetails.error);
  const isLoggedin = useSelector((state: Store) => state.auth.isLoggedin);

  const background = `https://image.tmdb.org/t/p/original/${movie.details?.backdrop_path}`;
  const voteAverage = movie.details?.vote_average;
  const voteAverageDecimal: any = parseFloat(voteAverage).toFixed(1);
  const voteAveragePercent = voteAverageDecimal * 10;
  const movieHomePage = movie.details?.homepage;

  return (
    <>
      {error ? (
        <div className="error-message">
          <p>Movie Not Found!</p>
        </div>
      ) : (
        <div
          className="background"
          style={{
            backgroundImage: `url('${background}')`,
          }}
        >
          <div className="content-wrapper">
            <div className="poster">
              <img
                src={`https://image.tmdb.org/t/p/w500/${movie.details?.poster_path}`}
                alt="poster"
                className="poster-img"
              />
            </div>
            <div className="details-content">
              <h1 className="movie-title">{movie.details?.title}</h1>
              <p className="date">{movie.details?.release_date}</p>
              {movieHomePage !== "" && (
                <a
                  href={movieHomePage}
                  target="_blank"
                  rel="noreferrer"
                  className="homepage-link"
                >
                  <IoIosLink />
                </a>
              )}
              <div className="inline">
                <div className="user-score">
                  <span>{`${voteAveragePercent}%`}</span>
                </div>
              </div>
              <div className="overview">
                <h2 className="titles">Overwiev</h2>
                <p className="txt">{movie.details?.overview}</p>
              </div>
              <p className="tagline">{movie.details?.tagline}</p>
              <div className="wrapper">
                <div className="genres">
                  <h2 className="titles">Genres</h2>
                  {movie.details?.genres.map((genre: Genres) => {
                    return (
                      <p key={genre.id} className="txt">
                        {genre.name}
                      </p>
                    );
                  })}
                </div>
                <div className="status">
                  <h2 className="titles">Status</h2>
                  {movie.details?.status === "" ? (
                    <p className="txt">-</p>
                  ) : (
                    <p className="txt">{movie.details?.status}</p>
                  )}
                </div>
                <div className="languages">
                  <h2 className="titles">Spoken Languages</h2>
                  {movie.details?.spoken_languages.length === 0 ? (
                    <p className="txt">-</p>
                  ) : (
                    movie.details?.spoken_languages.map(
                      (language: SpokenLanguages) => {
                        return (
                          <p key={language.iso_639_1} className="txt">
                            {language.english_name}
                          </p>
                        );
                      }
                    )
                  )}
                </div>
                <div className="budget">
                  <h2 className="titles">Budget</h2>
                  {movie.details?.budget === 0 ? (
                    <p className="txt">-</p>
                  ) : (
                    <NumberFormat
                      className="txt"
                      value={movie.details?.budget}
                      displayType={"text"}
                      thousandSeparator={true}
                      prefix={"$ "}
                    />
                  )}
                </div>
                <div className="revenue">
                  <h2 className="titles">Revenue</h2>
                  {movie.details?.revenue === 0 ? (
                    <p className="txt">-</p>
                  ) : (
                    <NumberFormat
                      className="txt"
                      value={movie.details?.revenue}
                      displayType={"text"}
                      thousandSeparator={true}
                      prefix={"$ "}
                    />
                  )}
                </div>
              </div>
              {isLoggedin && (
                <div className="movie-rating">
                  <h2 className="titles">Rate this movie: </h2>
                  <input
                    type="number"
                    min={0.5}
                    max={10}
                    step={0.5}
                    className="rating"
                    defaultValue={0.5}
                  />
                  <button className="rate-btn">Rate</button>
                </div>
              )}
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default Details;
