import { useSelector } from "react-redux";
import { Store } from "../index";
import "./LogIn.scss";

const Login = ({
  username,
  password,
  handleUsername,
  handlePassword,
  handleSubmit,
}) => {
  const statusCode = useSelector(
    (state: Store) => state.auth.error.status_code
  );
  const statusMessage = useSelector(
    (state: Store) => state.auth.error.status_message
  );

  return (
    <form className="outside" onSubmit={handleSubmit}>
      <div className="container">
        <div className="username">
          <label className="label">Username</label>
          <input
            type="text"
            placeholder="Enter Username"
            className="input"
            value={username}
            onChange={handleUsername}
          />
        </div>
        <div className="password">
          <label className="label">Password</label>
          <input
            type="password"
            placeholder="Enter Password"
            className="input"
            value={password}
            onChange={handlePassword}
          />
        </div>
        {statusCode === 0 ? "" : <p className="message">{statusMessage}</p>}
        <button type="submit" className="btn">
          Login
        </button>
      </div>
    </form>
  );
};

export default Login;
