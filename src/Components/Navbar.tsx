import { useDispatch, useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import { Store } from "../index";
import { logout } from "../redux/actions/action-creators-auth";
import Logo from "./logo.svg";
import "./Navbar.scss";

const Navbar = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const isLoggedin = useSelector((state: Store) => state.auth.isLoggedin);

  const handleClick = () => {
    if (isLoggedin === false) {
      navigate("/login");
    }

    if (isLoggedin === true) {
      navigate("/");
      dispatch(logout());
    }
  };

  return (
    <div className="nav">
      <Link to="/">
        <img src={Logo} alt="" className="logo" />
      </Link>
      <div className="login-logout">
        <button className="btn" onClick={handleClick}>
          {isLoggedin ? "Logout" : "Login"}
        </button>
      </div>
    </div>
  );
};

export default Navbar;
