import ReactPaginate from "react-paginate";
import "./Pagination.scss";

function Pagination({ handlePageClick }) {
  return (
    <div className="pagination">
      <ReactPaginate
        onPageChange={handlePageClick}
        pageCount={500}
        pageRangeDisplayed={3}
        breakLabel="..."
        nextLabel=">"
        previousLabel="<"
        containerClassName="pagination-container"
        pageClassName="page"
        activeClassName="active-page"
        previousClassName="prev"
        nextClassName="next"
        disabledClassName="disable"
        breakClassName="dots"
      />
    </div>
  );
}

export default Pagination;
