export const MOVIE_DETAILS_LOADING = "MOVIE_DETAILS_LOADING" as const;
export const MOVIE_DETAILS_SUCCESS = "MOVIE_DETAILS_SUCCESS" as const;
export const MOVIE_DETAILS_FAILURE = "MOVIE_DETAILS_FAILURE" as const;

export type Details = {
  backdrop_path: string;
  budget: number;
  genres: Genres[];
  homepage: string;
  id: number;
  overview: string;
  popularity: number;
  poster_path: string;
  release_date: string;
  revenue: number;
  spoken_languages: SpokenLanguages[];
  status: string;
  tagline: string;
  title: string;
  vote_average: number;
};

export type Genres = {
  id: number;
  name: string;
};

export type SpokenLanguages = {
  english_name: string;
  iso_639_1: string;
  name: string;
};

export interface MovieDetailsLoading {
  type: typeof MOVIE_DETAILS_LOADING;
}

export interface MovieDetailsSuccess {
  type: typeof MOVIE_DETAILS_SUCCESS;
  payload: Details;
}

export interface MovieDetailsFailure {
  type: typeof MOVIE_DETAILS_FAILURE;
  payload: string;
}

export type MovieDetailsTypes =
  | MovieDetailsLoading
  | MovieDetailsSuccess
  | MovieDetailsFailure
  | any;
