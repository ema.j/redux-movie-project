import axios, { AxiosError } from "axios";
import { Dispatch } from "react";
import {
  REQUEST_TOKEN,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  LOGIN,
  LOGOUT,
  Token,
  Login,
  LoginTypes,
} from "./action-types-auth";

// const API_KEY = process.env.REACT_APP_API_KEY;

const fetchToken = (token: Token) => ({
  type: REQUEST_TOKEN,
  payload: token,
});

const fetchLoginSuccess = (login: Login) => ({
  type: LOGIN_SUCCESS,
  payload: login,
});

const fetchLoadingFailure = (error: AxiosError) => ({
  type: LOGIN_FAILURE,
  payload: error,
});

export const getToken = () => async (dispatch: Dispatch<LoginTypes>) => {
  const response = await axios.get(
    `https://api.themoviedb.org/3/authentication/token/new?api_key=98c171129c30704b9723af9ad17f2ed8`
  );

  dispatch(fetchToken(response.data));
};

export const validateWithLogin =
  (username, password, request_token) =>
  async (dispatch: Dispatch<LoginTypes>) => {
    const user = {
      username: username,
      password: password,
      request_token: request_token,
    };

    try {
      // dispatch(logout());
      const response = await axios.post(
        `https://api.themoviedb.org/3/authentication/token/validate_with_login?api_key=98c171129c30704b9723af9ad17f2ed8`,
        { ...user }
      );

      dispatch(fetchLoginSuccess(response.data));
      // dispatch(login());
    } catch (err) {
      if (err instanceof AxiosError) {
        dispatch(fetchLoadingFailure(err.response?.data));
      }
    }
  };

export const login = () => ({
  type: LOGIN,
});

export const logout = () => ({
  type: LOGOUT,
});
