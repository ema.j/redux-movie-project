export const REQUEST_TOKEN = "REQUEST_TOKEN" as const;
export const LOGIN_SUCCESS = "LOGIN_SUCCESS" as const;
export const LOGIN_FAILURE = "LOGIN_FAILURE" as const;
export const LOGIN = "LOGIN" as const;
export const LOGOUT = "LOGOUT" as const;

export type Token = {
  request_token: string;
  success: boolean;
};

export type Login = {
  success: boolean;
};

export type AxiosError = {
  status_code: number;
  status_message: string;
  success: boolean;
};

export interface RequestToken {
  type: typeof REQUEST_TOKEN;
  payload: Token;
}

export interface LoginSuccess {
  type: typeof LOGIN_SUCCESS;
  payload: Login;
}

export interface LoginFailure {
  type: typeof LOGIN_FAILURE;
  payload: AxiosError;
}

export interface LoggedIn {
  type: typeof LOGIN;
}

export interface LoggedOut {
  type: typeof LOGOUT;
}

export type LoginTypes =
  | RequestToken
  | LoginSuccess
  | LoginFailure
  | LoggedIn
  | LoggedOut
  | any;
