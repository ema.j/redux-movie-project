import axios from "axios";
import { Dispatch } from "redux";
import {
  MOVIES_LOADING,
  MOVIES_SUCCESS,
  MOVIES_FAILURE,
  Movies,
  MoviesActionsTypes,
} from "./action-types-movies";

// const API_KEY = process.env.REACT_APP_API_KEY;

const fetchMoviesLoading = () => ({
  type: MOVIES_LOADING,
});

const fetchMoviesSuccess = (movie: Movies) => ({
  type: MOVIES_SUCCESS,
  payload: movie,
});

const fetchMoviesFailure = (error: string) => ({
  type: MOVIES_FAILURE,
  payload: error,
});

export const getMovies =
  (page: number) => async (dispatch: Dispatch<MoviesActionsTypes>) => {
    try {
      dispatch(fetchMoviesLoading());

      const response = await axios.get(
        `https://api.themoviedb.org/3/movie/popular?api_key=98c171129c30704b9723af9ad17f2ed8&page=${page}`
      );

      dispatch(fetchMoviesSuccess(response.data));
    } catch (err) {
      if (err instanceof Error) {
        dispatch(fetchMoviesFailure(err.message));
      }
    }
  };
