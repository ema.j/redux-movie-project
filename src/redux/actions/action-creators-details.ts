import axios from "axios";
import { Dispatch } from "react";
import {
  MOVIE_DETAILS_LOADING,
  MOVIE_DETAILS_SUCCESS,
  MOVIE_DETAILS_FAILURE,
  Details,
  MovieDetailsTypes,
} from "./action-types-details";

// const API_KEY = process.env.REACT_APP_API_KEY;

const fetchMovieDetailsLoading = () => ({
  type: MOVIE_DETAILS_LOADING,
});

const fetchMovieDetailsSuccess = (details: Details) => ({
  type: MOVIE_DETAILS_SUCCESS,
  payload: details,
});

const fetchMovieDetailsFailure = (error: string) => ({
  type: MOVIE_DETAILS_FAILURE,
  payload: error,
});

export const getMovieDetails =
  (id: number | any) => async (dispatch: Dispatch<MovieDetailsTypes>) => {
    try {
      dispatch(fetchMovieDetailsLoading());

      const response = await axios.get(
        `https://api.themoviedb.org/3/movie/${id}?api_key=98c171129c30704b9723af9ad17f2ed8`
      );

      dispatch(fetchMovieDetailsSuccess(response.data));
    } catch (err) {
      if (err instanceof Error) {
        dispatch(fetchMovieDetailsFailure(err.message));
        console.log(err.message);
      }
    }
  };
