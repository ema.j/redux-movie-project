export const MOVIES_LOADING = "MOVIES_LOADING" as const;
export const MOVIES_SUCCESS = "MOVIES_SUCCESS" as const;
export const MOVIES_FAILURE = "MOVIES_FAILURE" as const;

export type Movies = {
  page: number;
  results: MoviesResults[];
};

export type MoviesResults = {
  id: number;
  popularity: number;
  poster_path: string;
  release_date: string;
  title: string;
  vote_average: number;
};

export interface MoviesLoading {
  type: typeof MOVIES_LOADING;
}

export interface MoviesSuccess {
  type: typeof MOVIES_SUCCESS;
  payload: Movies;
}

export interface MovieFailure {
  type: typeof MOVIES_FAILURE;
  payload: string;
}

export type MoviesActionsTypes =
  | MoviesLoading
  | MoviesSuccess
  | MovieFailure
  | any;
