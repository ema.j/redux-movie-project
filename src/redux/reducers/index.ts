import { combineReducers } from "redux";
import authReducer from "./authReducer";
import detailsReducer from "./detailsReducer";
import moviesReducer from "./moviesReducer";

const reducers = combineReducers({
  allMovies: moviesReducer,
  movieDetails: detailsReducer,
  auth: authReducer,
});

export default reducers;
