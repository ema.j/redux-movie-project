import {
  MOVIE_DETAILS_LOADING,
  MOVIE_DETAILS_SUCCESS,
  MOVIE_DETAILS_FAILURE,
  Details,
  MovieDetailsTypes,
} from "../actions/action-types-details";

interface InitialState {
  loading: boolean;
  details?: Details;
  error?: string;
}

const initialState: InitialState = {
  loading: false,
  details: undefined,
  error: "",
};

const detailsReducer = (
  state: InitialState = initialState,
  action: MovieDetailsTypes
) => {
  switch (action.type) {
    case MOVIE_DETAILS_LOADING:
      return {
        loading: true,
      };
    case MOVIE_DETAILS_SUCCESS:
      return {
        loading: false,
        details: action.payload,
      };
    case MOVIE_DETAILS_FAILURE:
      return {
        loading: false,
        error: action.payload,
      };
    default:
      return state;
  }
};

export default detailsReducer;
