import {
  REQUEST_TOKEN,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  LOGIN,
  LOGOUT,
  Token,
  Login,
  AxiosError,
  LoginTypes,
} from "../actions/action-types-auth";

interface InitialState {
  token?: Token;
  login?: Login;
  error?: AxiosError;
  isLoggedin?: boolean;
  isLoggedout?: boolean;
}

const initialState: InitialState = {
  token: {
    request_token: "",
    success: false,
  },

  login: {
    success: false,
  },

  error: {
    status_code: 0,
    status_message: "",
    success: false,
  },

  isLoggedin: false,
  isLoggedout: true,
};

const authReducer = (
  state: InitialState = initialState,
  action: LoginTypes
) => {
  switch (action.type) {
    case REQUEST_TOKEN:
      return {
        ...state,
        token: action.payload,
        isLoggedin: false,
        isLoggedout: true,
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        login: action.payload,
        isLoggedin: true,
        isLoggedout: false,
      };
    case LOGIN_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoggedin: false,
        isLoggedout: true,
      };
    case LOGIN:
      return {
        isLoggedin: true,
        isLoggedout: false,
      };
    case LOGOUT:
      return {
        isLoggedin: false,
        isLoggedout: true,
      };
    default:
      return state;
  }
};

export default authReducer;
