import {
  MOVIES_FAILURE,
  MOVIES_LOADING,
  MOVIES_SUCCESS,
  MoviesActionsTypes,
  Movies,
} from "../actions/action-types-movies";

interface InitialState {
  loading?: boolean;
  movie?: Movies;
  error?: string;
}

const initialState: InitialState = {
  loading: false,
  movie: {
    page: 1,
    results: [],
  },
  error: "",
};

const moviesReducer = (
  state: InitialState = initialState,
  action: MoviesActionsTypes
) => {
  switch (action.type) {
    case MOVIES_LOADING:
      return {
        loading: true,
      };
    case MOVIES_SUCCESS:
      return {
        loading: false,
        movie: action.payload,
      };
    case MOVIES_FAILURE:
      return {
        loading: false,
        error: action.payload,
      };
    default:
      return state;
  }
};

export default moviesReducer;
