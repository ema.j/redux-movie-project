import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Store } from "../index";
import {
  getToken,
  login,
  validateWithLogin,
} from "../redux/actions/action-creators-auth";
import { LoginTypes } from "../redux/actions/action-types-auth";
import { useNavigate } from "react-router-dom";
import Login from "../Components/LogIn";

const LoginPage = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const token = useSelector((state: Store) => state.auth.token.request_token);
  const success = useSelector((state: Store) => state.auth.login.success);

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const handleUsername = (e: React.ChangeEvent<HTMLInputElement>) => {
    setUsername(e.target.value);
  };

  const handlePassword = (e: React.ChangeEvent<HTMLInputElement>) => {
    setPassword(e.target.value);
  };

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    dispatch(getToken() as LoginTypes);
    dispatch(validateWithLogin(username, password, token) as LoginTypes);

    if (success === true) {
      navigate("/");
      dispatch(login());
    }
  };

  return (
    <div>
      <Login
        username={username}
        password={password}
        handleUsername={handleUsername}
        handlePassword={handlePassword}
        handleSubmit={handleSubmit}
      />
    </div>
  );
};

export default LoginPage;
