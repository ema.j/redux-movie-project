import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { Store } from "../index";
import { getMovieDetails } from "../redux/actions/action-creators-details";
import { MovieDetailsTypes } from "../redux/actions/action-types-details";
import Details from "../Components/Details";

function DetailsPage() {
  const dispatch = useDispatch();

  const loading = useSelector((state: Store) => state.movieDetails.loading);

  const { movieId } = useParams();

  useEffect(() => {
    dispatch(getMovieDetails(movieId) as MovieDetailsTypes);
  }, [dispatch, movieId]);

  return (
    <>
      {loading ? (
        <div className="loader">
          <p>LOADING...</p>
        </div>
      ) : (
        <Details />
      )}
    </>
  );
}

export default DetailsPage;
