import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Store } from "../index";
import { MoviesActionsTypes } from "../redux/actions/action-types-movies";
import { getMovies } from "../redux/actions/action-creators-movies";
import Movies from "../Components/Movies";
import Pagination from "../Components/Pagination";

function HomePage() {
  const [page, setPage] = useState(1);
  const dispatch = useDispatch();

  const loading = useSelector((state: Store) => state.allMovies.loading);

  useEffect(() => {
    dispatch(getMovies(page) as MoviesActionsTypes);
  }, [dispatch, page]);

  const handlePageClick = (e: { selected: number }) => {
    setPage(e.selected + 1);
  };

  return (
    <>
      {loading ? (
        <div className="loader">
          <p>LOADING...</p>
        </div>
      ) : (
        <Movies />
      )}
      <Pagination handlePageClick={handlePageClick} />
    </>
  );
}

export default HomePage;
